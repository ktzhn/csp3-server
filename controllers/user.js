const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return User.find({email: reqBody.email}).then(result => {
		// Check if email exists in the Users collection, then allows when no user has the same email
		if(result.length === 0){
			console.log(`New user registered.`)
			return newUser.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
		})
		}
		else{
			console.log(`User already exists. Please use a different email.`)
			return false;
		}
	})
	
}

// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				console.log(`Login success. Access Token generated.`)
				return { access: auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

// Make a user an admin (ADMIN)
module.exports.toAdmin = async (user, reqParams, reqBody) => {
	if(user.isAdmin){
		let adminUser = {
			isAdmin : reqBody.isAdmin
		}

		return User.findByIdAndUpdate(reqParams.userId, adminUser).then((newuser, error) => {
			if(error){
				console.log(`Failed to update.`)
				return false;
			}
			else{
				console.log(`User changed to admin.`)
				return true;
			}
		})
	}
	else{
		return (`You have no authorization for admin access.`);
		return false;
	}
}

// user details
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.userId).then(result => {
		if(result == null){
			return false;
		}
		else{
			result.password = "";
			return result;
		}
	})
}