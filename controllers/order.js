const Order = require("../models/Order");

// Create new Order (non-admin)
module.exports.newOrder = async (user, reqBody) => {
	if(user.isAdmin === false){
		let newOrder = new Order({
			totalAmount: reqBody.totalAmount,
			orderProducts: reqBody.orderProducts,
			orderUserId: user.id,
		})

		return newOrder.save().then((order, error) => {
			if(error){
				return false;
			}
			else{
				console.log(`Order has been placed.`)
				return true;
			}
		})
	}
	else{
		console.log(`You are an admin. Cannot create new order.`)
		return false;
	}
}


// Retrieve authenticated user's order

module.exports.userOrder = async (user,reqParams) => {
	if(user.isAdmin === false){
		return Order.find({orderUserId : reqParams.userId}).then(result => {

				console.log(`Order found.`)
				return result;
		})
	}
	else{
		console.log(`You are an admin.`)
		return false;
	}
}


// Retrieve ALL orders (ADMIN)
module.exports.allOrder = async (user, reqBody) => {
	if(user.isAdmin === true){
		return Order.find({}).then(result => {
		return result;
	}) 
	}
	else{
		console.log(`You are not authorized to view that.`)
		return false;
	}
}

