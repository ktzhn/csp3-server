const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");



// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for changing user to admin
router.put("/admin/:userId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	userController.toAdmin(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode (req.headers.authorization);
	console.log(userData)
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})


// Allows us to export the "router" object that will be accessed in "index.js"
module.exports = router;